"""
This file is a "central controller", or main file, which is able to access and run other Python files in this directory dependant on user CLI input.
"""
import re

# These are the Python files to be referenced
module_dict = {}

# The keys in the help dictionary should match
help_args_dict = {
                "about": "This is a project to display my, Tango Khoohatong's, development practice.",
                "encryption": "This module can encrypt, decrypt, and output various strings.",
                "math": "This module contains various mathematical algorithms."
                }

def help(argument: str = ''):
    """
    This function takes in up to 1 argument which translates to a module to display a help message for.
    """
    if not argument: # User did not enter a string following help (i.e. just "help")
        for key in help_args_dict:
            print(f"{key}: {help_args_dict[key]}")
    else: # User entered more than just "help"
        if help_args_dict[argument]:
            print(help_args_dict[argument])
        else:
            print("Invalid help argument")
    print("\n")

def main():
    """
    This is the main function which has the main loop to run everything else. This is only responsible for reflectively accessing modules and the generic help options. Each file accessed has a responsibility to provide its own help text including callable functions.
    """
    while True:
        command: str = input("Enter the module for your command or \"help\":")
        print("\n")
        args = re.split(" ", command)
        if args[0] == "help":
            if len(args) > 2:
                print("Too many arguments")
            elif len(args) == 2:
                help(args[1])
            else:
                help()
        elif command in module_dict:
            #TODO
            pass
        

if __name__ == "__main__":
    main()