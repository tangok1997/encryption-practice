import string
import re

class CypherParamError(Exception):
    """An improper cypher param was passed"""
    def __init__(self, message: str) -> None:
        self.message = message
        super().__init__(message)

    def get_message(self) -> str:
        return self.message

# Start parameter validation helper functions

def _params_exist(plaintext: str, key: str) -> None: 
    """Throw a custom exception if a parameter is missing"""
    if not plaintext:
        raise CypherParamError("Missing plaintext to encrypt.")
    if not key:
        raise CypherParamError("Missing encryption key.")

def _params_english(plaintext: str, key: str) -> None:
    """Throw an exception if a parameter contains non-English characters."""
    if not re.match("^[a-zA-Z]*$", plaintext):
        raise CypherParamError("Plaintext contains non-english characters.")
    if not re.match("^[a-zA-Z]*$", key):
        raise CypherParamError("Key contains non-english characters.")

# End parameter validation helper functions

# Start Vigenere

def _alphabet_right_shift(word: str, shift_by: int) -> str:
    """
    Shift uppercase english alphabet by +1 ASCII value. This loops back to the beginning of the alphabet
    i.e. 
    AFZ -> BGA
    (In ASCII)
    65 70 90 -> 66 71 65
    """
    shifted_word: str = ""
    i: int = 0
    while i < len(word):
        shifted_word += chr((ord(word[i]) - 65 + shift_by) % 26 + 65)
        i += 1
    return shifted_word

def _create_vigenere_cypher() -> list[list[str]]:
    """
    ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'A']
    . . .
    ['Z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y']
    """
    alphabet: str = string.ascii_uppercase
    alphabet_length: int = len(alphabet)
    cypher: list[list[str]] = [['']] * alphabet_length
    i: int = 0
    while i < len(cypher):
        cypher[i] = list(_alphabet_right_shift(alphabet, i))
        i += 1
    return cypher

def _get_vigenere_key(plaintext: str, key: str) -> str:
    """Lengthens the key to be the same length as the plaintext if the key is shorter than the plaintext."""
    if len(key) < len(plaintext):
        full_key: str = key
        len_diff: int = len(plaintext) - len(key)
        for _ in range(int(len_diff / len(key))):
            full_key += key
        remainder: int = len_diff % len(key)
        full_key += key[:remainder:]
        key = full_key
    return key

def vigenere_encrypt(plaintext: str, key: str) -> str:
    """
    Use the Vigenere encryption algorithm to encrypt an English string and return the encrypted string.
    Note: Lower case characters will be converted to uppercase. Possible loss of data.
    """
    try:
        _params_exist(plaintext, key)
        _params_english(plaintext, key)
    except CypherParamError as e:
        return CypherParamError.get_message(e)

    cypher: list[list[str]] = _create_vigenere_cypher()
    true_key: str = _get_vigenere_key(plaintext,key)
    encrypted_text: str = ""
    for i in range(len(plaintext)):
        row: int = ord(true_key[i].upper()) - 65
        col: int = ord(plaintext[i].upper()) - 65
        encrypted_text += cypher[row][col]
    return encrypted_text

# End Vigenere

def main():
    plaintext = input("Enter plaintext to encrypt: ")
    key = input("Enter a key: ")
    print(vigenere_encrypt(plaintext, key))

if __name__ == "__main__":
    main()