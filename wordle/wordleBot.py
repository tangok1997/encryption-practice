from webbrowser import get
from wordData import WordData
import time


def getWordList(filePath):
    '''Get all lines from a file. Intended to be used on a file with individual words per line that includes only possible Wordle words.'''
    f = open(filePath, 'r')
    words = []
    for line in f:
        words.append(line.strip())
    return words


def checkWordAgainstWordData(newWord, oldWordData: WordData):
    '''Check if a newWord is possible based on oldWordData's pattern. Returns True if the word is possible, and False otherwise.'''
    pattern: list[int] = oldWordData.pattern
    for charIndex in range(len(pattern)):
        if pattern[charIndex] == WordData.VALUE_DICT["LETTER_NOT_FOUND"]:
            # Gray letter found in new word
            if oldWordData.word[charIndex] in newWord:
                return False
        elif pattern[charIndex] == WordData.VALUE_DICT["LETTER_ELSEWHERE"]:
            # Yellow letter not in new word
            if oldWordData.word[charIndex] not in newWord:
                return False
            # Yellow letter in the same location
            elif oldWordData.word[charIndex] == newWord[charIndex]:
                return False
        elif pattern[charIndex] == WordData.VALUE_DICT["LETTER_IN_PLACE"]:
            # Green letter not in the same location
            if oldWordData.word[charIndex] != newWord[charIndex]:
                return False
    return True


def getNewWordList(wordData: WordData, oldWordList: list[str]):
    '''Return a list of possible words from oldWordList after being filtered by wordData's Pattern object. '''
    newWordList: list[str] = []
    for word in oldWordList:
        if checkWordAgainstWordData(word, wordData):
            newWordList.append(word)
    return newWordList


def populateBit(wordData: WordData, oldWordList: list[str], newWordList: list[str]):
    '''Set the bit field of a WordData object'''
    wordData.bits = WordData.determineBits(oldWordList, newWordList)


def printBestWords(wordDataList: list[WordData], num: int):
    bestWords: list[WordData] = [WordData("", [], -1) for _ in range(num)]
    tmpList = wordDataList
    for i in range(num):
        for j in tmpList:
            if j.bits > bestWords[i].bits:
                bestWords[i] = j
    print(bestWords)

def getAverageBits(word, wordList):
    wordDataList: list[WordData] = []
    for patt in WordData.ALL_PATTERNS:
        newWordData = WordData(word, patt, 0)
        newWords = getNewWordList(newWordData, wordList)
        newWordData.bits = WordData.determineBits(wordList, newWords)
        wordDataList.append(newWordData)
    sumBits = 0
    for x in wordDataList:
        sumBits += x.bits
    return sumBits / len(wordDataList)


def fullRun():
    words = getWordList("words.txt")
    # List of words and their average bit value
    tupleList: list[tuple[float, str]] = []
    for x in range(len(words)):
        tup = ((getAverageBits(words[x], words), words[x]))
        f = open("unsortedFirstWords.txt", "a")
        f.write(f"{x + 1}. {tup[1]} {tup[0]},\n")
        f.close()
        tupleList.append(tup)
    for x in range(len(sorted(tupleList))):
        f = open("sortedFirstWords.txt", "w")
        f.write(f"{x + 1}. {tupleList[x][1]} {tupleList[x][0]},\n")
        f.close()


def sortCsv(fileToSort, sortFile):
    tupList: list[tuple[float, str]] = []
    with open(fileToSort, "r") as f:
        for line in f:
            entry = line.strip(",\n").split(" ")
            tup = (float(entry[2]), entry[1])
            tupList.append(tup)
    with open(sortFile, "w") as f:
        sortedTups = sorted(tupList, reverse=True)
        for tupIndex in range(len(sortedTups)):
            f.write(
                f"{tupIndex + 1}. {sortedTups[tupIndex][1]} {sortedTups[tupIndex][0]},\n")



def main():
    sortCsv("unsortedFirstWords.txt", "sortedFirstWords.txt")


if __name__ == "__main__":
    main()
