from functools import total_ordering
import math
from itertools import product


@total_ordering
class WordData:
    '''
    Class representing a word and its associated data.
    A pattern represents a list of ints corresponding to the color pattern obtained from inputting a wordle word.
    A single bit of information represents an observation which cuts the set of possibilities in half.
    '''
    VALUE_DICT = {"LETTER_NOT_FOUND": 0,
                 "LETTER_ELSEWHERE": 1,
                 "LETTER_IN_PLACE": 2}
    ALL_PATTERNS = [
                    [0, 0, 0, 0, 0], [0, 0, 0, 0, 1], [0, 0, 0, 0, 2], 
                    [0, 0, 0, 1, 0], [0, 0, 0, 1, 1], [0, 0, 0, 1, 2], 
                    [0, 0, 0, 2, 0], [0, 0, 0, 2, 1], [0, 0, 0, 2, 2], 
                    [0, 0, 1, 0, 0], [0, 0, 1, 0, 1], [0, 0, 1, 0, 2], 
                    [0, 0, 1, 1, 0], [0, 0, 1, 1, 1], [0, 0, 1, 1, 2], 
                    [0, 0, 1, 2, 0], [0, 0, 1, 2, 1], [0, 0, 1, 2, 2], 
                    [0, 0, 2, 0, 0], [0, 0, 2, 0, 1], [0, 0, 2, 0, 2], 
                    [0, 0, 2, 1, 0], [0, 0, 2, 1, 1], [0, 0, 2, 1, 2], 
                    [0, 0, 2, 2, 0], [0, 0, 2, 2, 1], [0, 0, 2, 2, 2], 
                    [0, 1, 0, 0, 0], [0, 1, 0, 0, 1], [0, 1, 0, 0, 2], 
                    [0, 1, 0, 1, 0], [0, 1, 0, 1, 1], [0, 1, 0, 1, 2], 
                    [0, 1, 0, 2, 0], [0, 1, 0, 2, 1], [0, 1, 0, 2, 2], 
                    [0, 1, 1, 0, 0], [0, 1, 1, 0, 1], [0, 1, 1, 0, 2], 
                    [0, 1, 1, 1, 0], [0, 1, 1, 1, 1], [0, 1, 1, 1, 2], 
                    [0, 1, 1, 2, 0], [0, 1, 1, 2, 1], [0, 1, 1, 2, 2], 
                    [0, 1, 2, 0, 0], [0, 1, 2, 0, 1], [0, 1, 2, 0, 2], 
                    [0, 1, 2, 1, 0], [0, 1, 2, 1, 1], [0, 1, 2, 1, 2], 
                    [0, 1, 2, 2, 0], [0, 1, 2, 2, 1], [0, 1, 2, 2, 2], 
                    [0, 2, 0, 0, 0], [0, 2, 0, 0, 1], [0, 2, 0, 0, 2], 
                    [0, 2, 0, 1, 0], [0, 2, 0, 1, 1], [0, 2, 0, 1, 2], 
                    [0, 2, 0, 2, 0], [0, 2, 0, 2, 1], [0, 2, 0, 2, 2], 
                    [0, 2, 1, 0, 0], [0, 2, 1, 0, 1], [0, 2, 1, 0, 2], 
                    [0, 2, 1, 1, 0], [0, 2, 1, 1, 1], [0, 2, 1, 1, 2], 
                    [0, 2, 1, 2, 0], [0, 2, 1, 2, 1], [0, 2, 1, 2, 2], 
                    [0, 2, 2, 0, 0], [0, 2, 2, 0, 1], [0, 2, 2, 0, 2], 
                    [0, 2, 2, 1, 0], [0, 2, 2, 1, 1], [0, 2, 2, 1, 2], 
                    [0, 2, 2, 2, 0], [0, 2, 2, 2, 1], [0, 2, 2, 2, 2], 
                    [1, 0, 0, 0, 0], [1, 0, 0, 0, 1], [1, 0, 0, 0, 2], 
                    [1, 0, 0, 1, 0], [1, 0, 0, 1, 1], [1, 0, 0, 1, 2], 
                    [1, 0, 0, 2, 0], [1, 0, 0, 2, 1], [1, 0, 0, 2, 2], 
                    [1, 0, 1, 0, 0], [1, 0, 1, 0, 1], [1, 0, 1, 0, 2], 
                    [1, 0, 1, 1, 0], [1, 0, 1, 1, 1], [1, 0, 1, 1, 2], 
                    [1, 0, 1, 2, 0], [1, 0, 1, 2, 1], [1, 0, 1, 2, 2], 
                    [1, 0, 2, 0, 0], [1, 0, 2, 0, 1], [1, 0, 2, 0, 2], 
                    [1, 0, 2, 1, 0], [1, 0, 2, 1, 1], [1, 0, 2, 1, 2], 
                    [1, 0, 2, 2, 0], [1, 0, 2, 2, 1], [1, 0, 2, 2, 2], 
                    [1, 1, 0, 0, 0], [1, 1, 0, 0, 1], [1, 1, 0, 0, 2], 
                    [1, 1, 0, 1, 0], [1, 1, 0, 1, 1], [1, 1, 0, 1, 2], 
                    [1, 1, 0, 2, 0], [1, 1, 0, 2, 1], [1, 1, 0, 2, 2], 
                    [1, 1, 1, 0, 0], [1, 1, 1, 0, 1], [1, 1, 1, 0, 2], 
                    [1, 1, 1, 1, 0], [1, 1, 1, 1, 1], [1, 1, 1, 1, 2], 
                    [1, 1, 1, 2, 0], [1, 1, 1, 2, 1], [1, 1, 1, 2, 2], 
                    [1, 1, 2, 0, 0], [1, 1, 2, 0, 1], [1, 1, 2, 0, 2], 
                    [1, 1, 2, 1, 0], [1, 1, 2, 1, 1], [1, 1, 2, 1, 2], 
                    [1, 1, 2, 2, 0], [1, 1, 2, 2, 1], [1, 1, 2, 2, 2], 
                    [1, 2, 0, 0, 0], [1, 2, 0, 0, 1], [1, 2, 0, 0, 2], 
                    [1, 2, 0, 1, 0], [1, 2, 0, 1, 1], [1, 2, 0, 1, 2], 
                    [1, 2, 0, 2, 0], [1, 2, 0, 2, 1], [1, 2, 0, 2, 2], 
                    [1, 2, 1, 0, 0], [1, 2, 1, 0, 1], [1, 2, 1, 0, 2], 
                    [1, 2, 1, 1, 0], [1, 2, 1, 1, 1], [1, 2, 1, 1, 2], 
                    [1, 2, 1, 2, 0], [1, 2, 1, 2, 1], [1, 2, 1, 2, 2], 
                    [1, 2, 2, 0, 0], [1, 2, 2, 0, 1], [1, 2, 2, 0, 2], 
                    [1, 2, 2, 1, 0], [1, 2, 2, 1, 1], [1, 2, 2, 1, 2], 
                    [1, 2, 2, 2, 0], [1, 2, 2, 2, 1], [1, 2, 2, 2, 2], 
                    [2, 0, 0, 0, 0], [2, 0, 0, 0, 1], [2, 0, 0, 0, 2], 
                    [2, 0, 0, 1, 0], [2, 0, 0, 1, 1], [2, 0, 0, 1, 2], 
                    [2, 0, 0, 2, 0], [2, 0, 0, 2, 1], [2, 0, 0, 2, 2], 
                    [2, 0, 1, 0, 0], [2, 0, 1, 0, 1], [2, 0, 1, 0, 2], 
                    [2, 0, 1, 1, 0], [2, 0, 1, 1, 1], [2, 0, 1, 1, 2], 
                    [2, 0, 1, 2, 0], [2, 0, 1, 2, 1], [2, 0, 1, 2, 2], 
                    [2, 0, 2, 0, 0], [2, 0, 2, 0, 1], [2, 0, 2, 0, 2], 
                    [2, 0, 2, 1, 0], [2, 0, 2, 1, 1], [2, 0, 2, 1, 2], 
                    [2, 0, 2, 2, 0], [2, 0, 2, 2, 1], [2, 0, 2, 2, 2], 
                    [2, 1, 0, 0, 0], [2, 1, 0, 0, 1], [2, 1, 0, 0, 2], 
                    [2, 1, 0, 1, 0], [2, 1, 0, 1, 1], [2, 1, 0, 1, 2], 
                    [2, 1, 0, 2, 0], [2, 1, 0, 2, 1], [2, 1, 0, 2, 2], 
                    [2, 1, 1, 0, 0], [2, 1, 1, 0, 1], [2, 1, 1, 0, 2], 
                    [2, 1, 1, 1, 0], [2, 1, 1, 1, 1], [2, 1, 1, 1, 2], 
                    [2, 1, 1, 2, 0], [2, 1, 1, 2, 1], [2, 1, 1, 2, 2], 
                    [2, 1, 2, 0, 0], [2, 1, 2, 0, 1], [2, 1, 2, 0, 2], 
                    [2, 1, 2, 1, 0], [2, 1, 2, 1, 1], [2, 1, 2, 1, 2], 
                    [2, 1, 2, 2, 0], [2, 1, 2, 2, 1], [2, 1, 2, 2, 2], 
                    [2, 2, 0, 0, 0], [2, 2, 0, 0, 1], [2, 2, 0, 0, 2], 
                    [2, 2, 0, 1, 0], [2, 2, 0, 1, 1], [2, 2, 0, 1, 2], 
                    [2, 2, 0, 2, 0], [2, 2, 0, 2, 1], [2, 2, 0, 2, 2], 
                    [2, 2, 1, 0, 0], [2, 2, 1, 0, 1], [2, 2, 1, 0, 2], 
                    [2, 2, 1, 1, 0], [2, 2, 1, 1, 1], [2, 2, 1, 1, 2], 
                    [2, 2, 1, 2, 0], [2, 2, 1, 2, 1], [2, 2, 1, 2, 2], 
                    [2, 2, 2, 0, 0], [2, 2, 2, 0, 1], [2, 2, 2, 0, 2], 
                    [2, 2, 2, 1, 0], [2, 2, 2, 1, 1], [2, 2, 2, 1, 2], 
                    [2, 2, 2, 2, 0], [2, 2, 2, 2, 1], [2, 2, 2, 2, 2]]

    def __init__(self, word: str, pattern: list[int], bits: float):
        self.word = word
        self.pattern = pattern
        self.bits = bits

    def __str__(self):
        return f"Word: {self.word}, Pattern: {self.pattern}, Bits: {self.bits}"

    def _is_valid_operand(self, other):
        return hasattr(other, "bits")

    def __eq__(self, __o) -> bool:
        if not self._is_valid_operand(__o):
            raise Exception("__o does not have a <bits> attribute")
        return self.bits == __o.bits

    def __lt__(self, __o) -> bool:
        if not self._is_valid_operand(__o):
            raise Exception("__o does not have a <bits> attribute")
        return self.bits < __o.bits

    @staticmethod
    def makeWordPattern(guessWord: str, answerWord: str):
        '''Return a Pattern object for the guessWord as compared to the answerWord'''
        pattern: list[int] = []
        for index in range(len(guessWord)):
            if guessWord[index] == answerWord[index]:
                pattern.append(
                    WordData.VALUE_DICT["LETTER_IN_PLACE"])
            elif guessWord[index] in answerWord:
                pattern.append(
                    WordData.VALUE_DICT["LETTER_ELSEWHERE"])
            else:
                pattern.append(
                    WordData.VALUE_DICT["LETTER_NOT_FOUND"])
        return pattern

    @staticmethod
    def getAllPatterns():
        '''
        Return a list of all possible permutations of wordle patterns.
        This should only need to be called once and then stored.
        '''
        patts = product([0, 1, 2], repeat=5)
        listPatts = list(patts)
        for tup in listPatts:
            list(tup)
        return listPatts

    @staticmethod
    def determineBits(originalSet, newSet):
        '''
        Assuming an observation was made that cut originalSet down to newSet,
        return how many bits of information that observation provided.
        '''
        ret = 0
        try:
            ret = math.log(len(newSet)/len(originalSet), 2) * -1.0000000
        except Exception:
            # newSetLen is 0 so the pattern yielded no possible words.
            pass
        return ret
