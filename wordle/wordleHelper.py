'''Assistant script for playing Wordle. '''
def FindWordsContaining(file):
    letters = input("Enter commas-separated letters to search for: ").split(',')
    f = open(file, 'r')
    goodWords = []
    for line in f:
        goodWord = True
        for char in letters:
            if char.strip() not in line:
                goodWord = False
                break
        if goodWord:
            goodWords.append(line.strip())
    return goodWords


def FindWordsExcluding(file):
    letters = input("Enter commas-separated letters to exclude: ").split(',')
    f = open(file, 'r')
    goodWords = []
    for line in f:
        goodWord = True
        for char in letters:
            if char.strip() in line:
                goodWord = False
                break
        if goodWord:
            goodWords.append(line.strip())
    return goodWords
        

if __name__ == '__main__':
    siteList = {0:'words.txt', 1:'wordlegamewords.txt'}
    whichSite = int(input("Enter 0 for wordle, or 1 for wordlegame: \r\n"))
    file = ""
    if whichSite not in siteList:
        print("Invalid entry")
    else:
        file = siteList[whichSite]
        methodIndex = input("Enter 0 for excludes and 1 for includes: \r\n")
        output = []
        if methodIndex == '0':
            output = FindWordsExcluding(file)
        elif methodIndex == '1':
            output = FindWordsContaining(file)
        else:
            print("Invalid entry")
        if len(output):
            output.sort()
            for word in output:
                print(word)
    